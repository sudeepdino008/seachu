package com.init;

import com.log.LoggerFactory;
import com.properties.SeachuProperties;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author sudeep
 * @since 31/08/16
 */
public class HelloServlet extends HttpServlet {

    Logger LOGGER = LoggerFactory.getLogger(HelloServlet.class);
    private static String DEFAULT_MESSAGE = "default";

    @Override
    public void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse){

            if("true".equals(httpServletRequest.getParameter("load"))){
            try {
                SeachuProperties.getSeachuInstance();
                OutputStream out = httpServletResponse.getOutputStream();
                out.write(Files.readAllBytes(Paths.get("/mnt1/seachu/test_pics/_3649262294.jpg")));
                out.write(Files.readAllBytes(Paths.get("/mnt1/seachu/test_pics/convo.jpg")));

            } catch (IOException e) {
                LOGGER.error("Cannot get output stream");
            }
            httpServletResponse.setHeader("Content-Type", "image/jpeg");
        }
        else {
            PrintWriter printWriter = null;
            try {
                printWriter = httpServletResponse.getWriter();
                LOGGER.error("Starting3");
            } catch (Exception e) {
                e.printStackTrace();
            }
            httpServletResponse.setContentType("text/html");
            SearchService searchResultService = new SearchService();
            String searchResult = searchResultService.getSearchResult(getSearchQuery(httpServletRequest.getQueryString()));
            writeResult(searchResult, printWriter);
            printWriter.close();
        }
    }

    private void writeResult(String result, PrintWriter printWriter){
        printWriter.print(String.format("<html><body><p>%s</p></html></body>", result));
    }

    private String getSearchQuery(String query){
        //if(StringUtils)
        if(StringUtils.isEmpty(query)){
            return DEFAULT_MESSAGE;
        }
        String[] tokens = query.split("query=");
        if(tokens.length>0 && StringUtils.isNotEmpty(tokens[1])){
            return tokens[1];
        }
        return DEFAULT_MESSAGE;
    }


}
