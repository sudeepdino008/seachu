package com.log;

import org.slf4j.ILoggerFactory;
import org.slf4j.Logger;

/**
 * @author sudeep
 * @since 02/09/16
 **/
public class LoggerFactory {
    public static final ILoggerFactory LOGGER_FACTORY = org.slf4j.LoggerFactory.getILoggerFactory();

    public static Logger getLogger(Class clazz){
        return LOGGER_FACTORY.getLogger(clazz.getName());
    }
}
