package com.properties;

/**
 * @author sudeep
 * @since 28/09/16
 */
public abstract class AbstractPropertiesWrapper implements PropertiesWrapper{

    @Override
    public String getString(String key, String defaultValue) {
        String value = getProperty(key);
        if(value==null) {
            return defaultValue;
        }
        return value;
    }

    @Override
    public Integer getInteger(String key, Integer defaultValue) {
        String value = getProperty(key);
        if(value==null) {
            return defaultValue;
        }
        return Integer.valueOf(value);
    }

    @Override
    public Long getLong(String key, Long defaultValue) {
        String value = getProperty(key);
        if(value==null) {
            return defaultValue;
        }
        return Long.valueOf(value);
    }

    @Override
    public Boolean getBoolean(String key, Boolean defaultValue) {
        String value = getProperty(key);
        if(value==null) {
            return defaultValue;
        }
        return Boolean.valueOf(value);
    }

    @Override
    public String getString(String key) {
        return getProperty(key);
    }

    @Override
    public Integer getInteger(String key) {
        return getInteger(key, null);
    }

    @Override
    public Long getLong(String key) {
        return getLong(key, null);
    }

    @Override
    public Boolean getBoolean(String key) {
        return getBoolean(key, null);
    }
}
