package com.properties;

/**
 * @author sudeep
 * @since 28/09/16
 */
public interface BasePropertiesWrapper {

    String getString(String key);

    Integer getInteger(String key);

    Long getLong(String key);

    Boolean getBoolean(String key);
}
