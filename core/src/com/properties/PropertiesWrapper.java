package com.properties;

/**
 * @author sudeep
 * @since 28/09/16
 */
public interface PropertiesWrapper extends BasePropertiesWrapper{

    String getString(String key, String defaultValue);

    String getProperty(String key);

    Integer getInteger(String key, Integer defaultValue);

    Long getLong(String key, Long defaultValue);

    Boolean getBoolean(String key, Boolean defaultValue);
}
