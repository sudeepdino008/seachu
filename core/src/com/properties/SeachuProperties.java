package com.properties;

import com.log.LoggerFactory;
import org.slf4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author sudeep
 * @since 28/09/16
 */
public class SeachuProperties extends AbstractPropertiesWrapper {
    private static Logger LOG = LoggerFactory.getLogger(SeachuProperties.class);
    private static Properties props = null;
    private static Properties properties;
    private static final String SEACHU_PROPERTIES_FILE = "/resources/seachu.properties";


    private SeachuProperties(Properties properties) {
        this.properties = properties;
    }

    public static Properties getInstance() {
        if (props == null) {
            loadProperties();
        }
        return props;
    }

    public static SeachuProperties getSeachuInstance(){
        return SeachuPropertiesHolder.ourInstance;
    }

    private static synchronized void loadProperties() {
        InputStream stream = SeachuProperties.class.getResourceAsStream(SEACHU_PROPERTIES_FILE);
        try {
            props = new Properties();
            props.load(stream);
        } catch (IOException e) {
            LOG.error("failed to load properties file:" + SEACHU_PROPERTIES_FILE);
            throw new RuntimeException("failed to load properties file");
        }
    }

    @Override
    public String getProperty(String key) {
        return properties.getProperty(key);
    }

    private static class SeachuPropertiesHolder {
        public static SeachuProperties ourInstance = new SeachuProperties(SeachuProperties.getInstance());
    }
}
