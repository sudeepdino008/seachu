package com.rest;

import org.glassfish.jersey.server.ResourceConfig;

import javax.annotation.PostConstruct;
import javax.servlet.ServletConfig;
import javax.ws.rs.core.Context;

/**
 * @author sudeep
 * @since 04/10/16
 */
public class CommonJerseyApplication extends ResourceConfig {
    public static final String PACKAGES_PARAM = "com.seachu.rest.packages";

    @Context
    private ServletConfig config;

    @PostConstruct
    public void init(){
        String packages = config.getInitParameter(PACKAGES_PARAM);
        packages(packages);
    }
}
