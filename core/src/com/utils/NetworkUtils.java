package com.utils;

import com.log.LoggerFactory;
import org.slf4j.Logger;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author sudeep
 * @since 04/10/16
 */
public class NetworkUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(NetworkUtils.class);

    public static URL unsafeUrl(String path) {
        URL url = null;
        try {
            url = new URL(path);
        } catch (MalformedURLException mue) {
            LOGGER.error("Malformed url:"+url.toString());
            throw new RuntimeException(mue);
        }
        return url;
    }

    public static URL safeUrl(String path){
        URL url = null;
        try {
            url = new URL(path);
        } catch(MalformedURLException mue){
            LOGGER.error("Malformed url:"+url.toString());
            //eat
        }
        return url;
    }
}
