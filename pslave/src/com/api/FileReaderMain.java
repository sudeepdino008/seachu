package com.api;

import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.nio.channels.FileChannel;
import java.nio.channels.SocketChannel;

/**
 * @author sudeep
 * @since 20/10/16
 */
public class FileReaderMain {
    public static int nbytes = 5154557, iter = 500;
    public static SocketChannel sc;


    public static void main(String[] args) throws IOException {
        sc = SocketChannel.open();
        sc.configureBlocking(true);
        sc.connect(new InetSocketAddress(6600));
        long start, sumz = 0, sumt = 0;
        for(int i = 0; i<iter; i++) {
            start = System.currentTimeMillis();
            zerocopy();
            sumz += System.currentTimeMillis() - start;
        }

        for(int i = 0; i<iter; i++) {
            start = System.currentTimeMillis();
            traditional();
            sumt += System.currentTimeMillis() - start;
        }
        System.out.println("sumz, sumt"+sumz+" "+sumt);
    }

    private static void zerocopy() throws IOException {
        FileInputStream is = new FileInputStream("/mnt1/file1.txt");
        FileChannel fileChannel = is.getChannel();
        fileChannel.transferTo(0, nbytes, sc);
        is.close();
    }

    private static void traditional() throws IOException {
        DataOutputStream dos = new DataOutputStream(sc.socket().getOutputStream());
        InputStream file = new FileInputStream("/mnt1/file1.txt");
        byte[] b = new byte[nbytes];
        file.read(b);
        dos.write(b);
        file.close();
    }
}