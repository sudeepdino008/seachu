package com.api;


import com.details.SlaveConfiguration;
import com.details.VolumeDetails;
import com.utils.NetworkUtils;
import com.utils.PSlaveConstants;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * @author sudeep
 * @since 01/10/16
 */

@Path("/pslave")
@Component
public class PSlaveRestAPI {

    @GET
    @Path("/details/volume")
    @Produces(MediaType.APPLICATION_JSON)
    public VolumeDetails volumeDetails(){
        String volume = SlaveConfiguration.getInstance().getProperty(PSlaveConstants.VOLUME);
        return VolumeDetails.getNewInstance().url(NetworkUtils.unsafeUrl(volume));
    }
}
