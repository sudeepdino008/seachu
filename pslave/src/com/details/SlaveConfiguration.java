package com.details;

import com.log.LoggerFactory;
import com.properties.AbstractPropertiesWrapper;
import com.properties.SeachuProperties;
import org.slf4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author sudeep
 * @since 28/09/16
 */
public class SlaveConfiguration extends AbstractPropertiesWrapper {
    private static Logger LOG = LoggerFactory.getLogger(SlaveConfiguration.class);
    private static Properties props = null;
    private static Properties properties;
    private static final String SLAVE_CONFIGURATION_FILE = "/resources/pslave.config";

    private SlaveConfiguration(Properties properties) {
        this.properties = properties;
    }

    public static Properties getInstance() {
        if (props == null) {
            loadProperties();
        }
        return props;
    }

    public static SlaveConfiguration getSlaveConfigInstance(){
        return SlaveConfigurationHolder.ourInstance;
    }

    private static synchronized void loadProperties() {
        InputStream stream = SlaveConfiguration.class.getResourceAsStream(SLAVE_CONFIGURATION_FILE);
        try {
            props = new Properties();
            props.load(stream);
        } catch (IOException e) {
            LOG.error("failed to load properties file:" + SLAVE_CONFIGURATION_FILE);
            throw new RuntimeException("failed to load properties file");
        }
    }

    @Override
    public String getProperty(String key) {
        return properties.getProperty(key);
    }

    private static class SlaveConfigurationHolder {
        public static SlaveConfiguration ourInstance = new SlaveConfiguration(SeachuProperties.getInstance());
    }
}
