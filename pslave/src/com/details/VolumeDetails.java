package com.details;

import java.net.URL;

/**
 * Represents the details of a particular volume.
 * @author sudeep
 * @since 04/10/16
 */
public class VolumeDetails {

    //reprsents the volume location. Could be remote
    private URL url = null;

    //if the volume is remote or local
    private boolean isRemote = false;

    //amount of data contained by this volume (in KB)
    private long size = 0;

    public VolumeDetails url(URL url){
        this.url = url;
        return this;
    }

    public VolumeDetails isRemote(boolean isRemote){
        this.isRemote = isRemote;
        return this;
    }

    public VolumeDetails size(long size){
        this.size = size;
        return this;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public boolean getIsRemote() {
        return isRemote;
    }

    public void setIsRemote(boolean isRemote) {
        this.isRemote = isRemote;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public static VolumeDetails getNewInstance(){
        return new VolumeDetails();
    }
}
